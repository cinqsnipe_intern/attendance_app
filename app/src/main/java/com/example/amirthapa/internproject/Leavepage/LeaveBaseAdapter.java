package com.example.amirthapa.internproject.Leavepage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.List;


/**
 * Created by amir thapa on 2/6/2017.
 */

public class LeaveBaseAdapter extends BaseAdapter {

    Context context;
    List<LeavesRowitems> rowitemses;


    public LeaveBaseAdapter(Context context, List<LeavesRowitems> rowitemses) {

        this.context = context;
        this.rowitemses = rowitemses;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.leavelist, viewGroup, false);
        TextView title, description, date;
        title = (TextView) view.findViewById(R.id.leavetitle);
        description = (TextView) view.findViewById(R.id.leavediscription);
        date = (TextView) view.findViewById(R.id.leavedate);

        LeavesRowitems leavesRowitems = rowitemses.get(i);
        title.setText(leavesRowitems.getTitle());
        description.setText(leavesRowitems.getDescription());
        date.setText(leavesRowitems.getDate());



        return view;

    }

    public LeaveBaseAdapter() {
        super();
    }


    @Override
    public int getCount() {
        return rowitemses.size() ;
    }

    @Override
    public Object getItem(int i) {
        return rowitemses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}


