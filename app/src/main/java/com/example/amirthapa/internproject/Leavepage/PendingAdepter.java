package com.example.amirthapa.internproject.Leavepage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by amir thapa on 3/8/2017.
 */

public class PendingAdepter extends ArrayAdapter<PendingModelclass> {
    Calendar calander;
    SimpleDateFormat simpledateformat;
    String Date;

    public PendingAdepter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public PendingAdepter(Context context, int resource, ArrayList<PendingModelclass> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.leavelist, null);
        }

        PendingModelclass p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.leavetitle);
            TextView tt2 =  (TextView)v.findViewById(R.id.leavediscription);
            TextView tt3 =(TextView)v.findViewById(R.id.leavedate);





            if(tt3!= null){
                calander = Calendar.getInstance();
                simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
                Date = simpledateformat.format(calander.getTimeInMillis());
            tt3.setText(Date);
                    }

            if (tt1 != null) {

                tt1.setText(p.getPendingtitle());
            }
            if(tt2 != null){
                tt2.setText(p.getPendingdescripion());
            }

        }

        return v;
    }

}

