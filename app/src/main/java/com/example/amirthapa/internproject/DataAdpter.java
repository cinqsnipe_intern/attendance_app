package com.example.amirthapa.internproject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static java.security.AccessController.getContext;

/**
 * Created by amir thapa on 3/7/2017.
 */

public class DataAdpter extends RecyclerView.Adapter<DataAdpter.ViewHolder> {
    private ArrayList<AndroidVerson> android;

Context context;


    public DataAdpter(Context contex,ArrayList<AndroidVerson> data) {
        this.android=data;
        this.context=contex;


    }

    //public void DataAdapter(ArrayList<AndroidVerson> android) {
      //  this.android = android;
    //}

    @Override
    public DataAdpter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notificationlist, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdpter.ViewHolder viewHolder, int i) {

        viewHolder.title.setText(android.get(i).getTitle());
        viewHolder.description.setText(android.get(i).getDescription());
//        Context context = viewHolder.ntimage.getContext();
      Picasso.with(context).load(android.get(i).getImage()).into(viewHolder.ntimage);
//        Picasso.with(context).load(android.get(i).getImage()).into(viewHolder.ntimage);
//        viewHolder.tv_api_level.setText(android.get(i).getRoll());
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title,description;
        private ImageView ntimage;
        public ViewHolder(View view) {
            super(view);

            title = (TextView)view.findViewById(R.id.noticetitle);
            description = (TextView)view.findViewById(R.id.noticedes);
            ntimage=(ImageView)view.findViewById(R.id.noticeimg);


        }
    }


    }

