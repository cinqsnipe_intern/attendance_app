package com.example.amirthapa.internproject.Homepage;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.List;

/**
 * Created by amir thapa on 2/9/2017.
 */

public class NotificationListAdpt extends ArrayAdapter<Notificationdata> {

    Context context;

    public NotificationListAdpt(Context context, int resourceId,
                               List<Notificationdata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        NotificationListAdpt.ViewHolder holder = null;
        Notificationdata notificationdata = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.notificationlist, null);
            holder = new NotificationListAdpt.ViewHolder();
            holder.txtDesc = (TextView) convertView.findViewById(R.id.noticedes);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.noticetitle);
            holder.imageView = (ImageView) convertView.findViewById(R.id.noticeimg);
            convertView.setTag(holder);
        } else
            holder = (NotificationListAdpt.ViewHolder) convertView.getTag();

        holder.txtDesc.setText(notificationdata.getDesc());
        holder.txtTitle.setText(notificationdata.getTitle());
        holder.imageView.setImageResource(notificationdata.getImageId());

        return convertView;
    }
}
