package com.example.amirthapa.internproject.Homepage;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.example.amirthapa.internproject.AndroidVerson;
import com.example.amirthapa.internproject.DataAdpter;
import com.example.amirthapa.internproject.JSONResponse;
import com.example.amirthapa.internproject.R;
import com.example.amirthapa.internproject.RequestInterface;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Notification extends AppCompatActivity {


ProgressDialog progressDialog;

//    public static final String[] titles = new String[] { "amir thapa is abest now ",
//            "sujan thapa is present now", "ram thapa is in leave now", "shyam thapa" };
//
//    public static final String[] descriptions = new String[] {
//            "days.if you do that it will be good for him. hopping for your positive respose",
//            "leave for a 3 days.if you do that it will be good for him.hopping for your positive respos", "hia leave for a 3 days.if you do that it will be good for him.hopping for your positive ",
//            "ceoabin has  leave for a 3 days.if you do that it will be good for him.hopping for your " };
//
//    public static final Integer[] images = { R.drawable.cover,
//            R.drawable.cover, R.drawable.cover, R.drawable.cover };
//
//    ListView notflistView;
//    List<Notificationdata> rowItems;

    private RecyclerView recyclerView;
    private  ArrayList<AndroidVerson>data;
    //    public ArrayList<AndroidVerson> data;
    private DataAdpter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        //comment gareko
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        fab.hide();


        initViews();


//        rowItems = new ArrayList<Notificationdata>();
//        for (int i = 0; i < titles.length; i++) {
//            Notificationdata item = new Notificationdata(images[i], titles[i], descriptions[i]);
//            rowItems.add(item);
//        }
//
//        notflistView = (ListView) findViewById(R.id.notificationlist);
//        NotificationListAdpt adapter = new NotificationListAdpt(this,
//                R.layout.notificationlist, rowItems);
//        notflistView.setAdapter(adapter);
//
//    }
    }
    private void initViews(){
        recyclerView = (RecyclerView)findViewById(R.id.notificationlist);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }



    private void loadJSON(){
        progressDialog=new ProgressDialog(Notification.this);
        progressDialog.setTitle("Loading..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.d("sown","sown");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.100.4:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<JSONResponse> call = request.getJSON();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
progressDialog.dismiss();
                JSONResponse jsonResponse = response.body();
//                data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));
                data=new ArrayList<>(Arrays.asList(jsonResponse.getNoticejson()));
                adapter =new DataAdpter(getApplicationContext(),data);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("Error",t.getMessage());
            }
        });
    }



    @Override
    public  void onBackPressed(){

        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
