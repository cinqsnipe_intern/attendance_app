package com.example.amirthapa.internproject.Leavepage;

/**
 * Created by amir thapa on 2/6/2017.
 */

public class LeavesRowitems {


    private  String title;
    private  String description;
    private String date;

    public LeavesRowitems(String title, String desc,String date) {

        this.title = title;
        this.description = description; }

    public LeavesRowitems(String title) {

        this.title = title;
    }
    public LeavesRowitems() {
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getDate() {
        return date;
    }


    @Override
    public String toString() {
        return title + "\n" + description+"\n" + date;
    }
}
