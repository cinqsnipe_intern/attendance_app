package com.example.amirthapa.internproject.Todopage;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amirthapa.internproject.Homepage.MainActivity;
import com.example.amirthapa.internproject.Homepage.Notification;
import com.example.amirthapa.internproject.Leavepage.Leaverequest;
import com.example.amirthapa.internproject.Profilepage.Profile;
import com.example.amirthapa.internproject.R;
import com.example.amirthapa.internproject.Reportpage.Report;

import java.util.ArrayList;


public class Todolist extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout clickdashbord;
    private  RelativeLayout clickprofile;
    private RelativeLayout clickleave;
    private RelativeLayout clickreport;
    private LinearLayout listcreate;

    ListView listView;
 ArrayList<String> list = new ArrayList<String>();
    RecyclerAdapter adapter;
    DatabaseHelpher helpher;
    ArrayList<DatabaseModel> todolistArrayList = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todolist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        listView = (ListView) findViewById(R.id.listView);
        helpher = new DatabaseHelpher(Todolist.this);

        listcreate =(LinearLayout)findViewById(R.id.createlist);
        listcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Showdilog();
            }
        });



        clickdashbord =(RelativeLayout)findViewById(R.id.dashbord) ;  // home onclick
        clickdashbord.setOnClickListener(this);

        clickprofile =(RelativeLayout)findViewById(R.id.profile) ;  // todolist onclick
        clickprofile.setOnClickListener(this);

        clickleave =(RelativeLayout)findViewById(R.id.leave) ;  //  leave  onclick
        clickleave.setOnClickListener(this);

        clickreport=(RelativeLayout)findViewById(R.id.Reports); //reports
        clickreport.setOnClickListener(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Showdilog();
            }
        });

        Bootomweight();// for height of the bottom menu bar


    }
    public  void Bootomweight(){
        Display display =  getWindowManager().getDefaultDisplay(); // for height of the bottom menu bar
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int screen_height = outMetrics.heightPixels;
        View bottom = findViewById(R.id.bottom);
        bottom.getLayoutParams().height = screen_height * 1/10;
    }


    private void Showdilog() {
        //dilog popup
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        final Dialog dialog = new Dialog(Todolist.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.addtodolist);
        TextView cancle = (TextView) dialog.findViewById(R.id.cancle);
        final EditText edit = (EditText) dialog.findViewById(R.id.editext);

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                dialog.dismiss();
            }
        });

        TextView add = (TextView) dialog.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String listtite = edit.getText().toString();
                DatabaseModel databaseModel = new DatabaseModel();

                if (listtite.equals("")) {
                    Toast.makeText(Todolist.this, "Please fill all the fields", Toast.LENGTH_LONG).show();
                } else {

                     imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                    databaseModel.setTodoTitle(listtite);
                    helpher.insertIntoDB(listtite);
                    adapter.add(databaseModel);
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();

                    onResume();

                }


                Toast.makeText(Todolist.this, "Sucessfully added to list", Toast.LENGTH_LONG);



            }
        });

        dialog.show();


    }

    @Override
    protected void onResume() {
        super.onResume();
        todolistArrayList = helpher.getDataFromDB();
        adapter = new RecyclerAdapter(this, R.layout.item_row, todolistArrayList);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Todolist.this, TodoContentlist.class);
                Log.d("the title is",todolistArrayList.get(position).getTodoTitle());
                intent.putExtra(TodoContentlist.TITLE, todolistArrayList.get(position).getTodoTitle());
                intent.putExtra(TodoContentlist.ID,todolistArrayList.get(position).getId());
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final Dialog dialogdelete = new Dialog(Todolist.this);

                dialogdelete.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogdelete.setContentView(R.layout.listdeletedilog);
                TextView cancledelete = (TextView) dialogdelete.findViewById(R.id.deletecancle);
                TextView ldelete = (TextView) dialogdelete.findViewById(R.id.delete);
                cancledelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogdelete.dismiss();
                    }
                });
                ldelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseModel databaseModel=todolistArrayList.get(position);
                        int id=todolistArrayList.get(position).getId();
                        helpher.deleteARow(id);
                        //todolistArrayList.remove(position);
                        adapter.remove(databaseModel);
                        //Intent intent=new Intent(Todolist.this,Todolist.class);
                        //startActivity(intent);
                        dialogdelete.dismiss();

                    }
                });
                dialogdelete.show();


                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.todo_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {

        } else if (id == R.id.todonotification) {
            Intent intent = new Intent(getApplicationContext(), Notification.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);


    }

    @Override
    public void onClick(View v) {
        if (v == clickdashbord) {
            Intent intent=new Intent(Todolist.this,MainActivity.class);
            startActivity(intent);

        } else if (v == clickprofile) {
            Intent intent=new Intent(Todolist.this,Profile.class);
            startActivity(intent);

        }
        else if (v == clickreport) {
            Intent intent=new Intent(Todolist.this,Leaverequest.class);
            startActivity(intent);

        }
        else if (v == clickleave) {
            Intent intent=new Intent(Todolist.this,Leaverequest.class);
            startActivity(intent);

        }

    }
}
