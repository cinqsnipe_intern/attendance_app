package com.example.amirthapa.internproject.Leavepage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class RejectFragment extends Fragment {

    ArrayList<LeavesRowitems> rowitemsr;
    LeavesRowitems rowitemr;
    String[] rtitle={"sickness","homework","personalwork"};
    String[] rdate={"jan 14-jan 16","jan 14-jan 16","jan 14-jan 16"};
    String[] rdescription={"Register your mobile to eSewa sljfg sg asdjf a;slkdjf as;dkfja a;slkdjf a;lskdfj a;sldkfj as;doei a;sdlkfja eo a;sdkf 'dfjg asdj as;ldj a","Know About eSewa falksdjfa alskdjf a;sldkfja sd;flk" ,"Know  alksdjf a;lskdjf a;sldkf a;skdlfj asdfAbout Fonepay" };
    ListView rlistView;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reject, container, false);


        rlistView=(ListView) v.findViewById(R.id.listviewr);


        rowitemsr= new ArrayList<>();
        for (int i=0;i<rtitle.length;i++)
        {
            rowitemr=new LeavesRowitems();

            rowitemr.setTitle(rtitle[i]);
            rowitemr.setDate(rdate[i]);
            rowitemr.setDescription(rdescription[i]);
            rowitemsr.add(rowitemr);

            LeaveBaseAdapter leaveBaseAdapter= new LeaveBaseAdapter(getContext(),rowitemsr);
           rlistView.setAdapter(leaveBaseAdapter);

        }
        return (v);
    }

}
