package com.example.amirthapa.internproject.Homepage;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;

/**
 * Created by amir thapa on 2/26/2017.
 */

public class Holidayslistadapter extends RecyclerView.Adapter<Holidayslistadapter.MyViewHolder> {
    private ArrayList<HolidaysArray> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView dateText;

        TextView monthText;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.dateText = (TextView) itemView.findViewById(R.id.date);
            //this.textViewVersion = (TextView) itemView.findViewById(R.id.textViewVersion);
            this.monthText = (TextView) itemView.findViewById(R.id.month);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public Holidayslistadapter(ArrayList<HolidaysArray> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.historyitem, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView dateText = holder.dateText;
        // TextView textViewVersion = holder.textViewVersion;
        TextView monthText = holder.monthText;

        dateText.setText(dataSet.get(listPosition).getDate());
        //textViewVersion.setText(dataSet.get(listPosition).getVersion());
        monthText.setText(dataSet.get(listPosition).getMonth());
    }



    @Override
    public int getItemCount() {
        return dataSet.size();
    }}

