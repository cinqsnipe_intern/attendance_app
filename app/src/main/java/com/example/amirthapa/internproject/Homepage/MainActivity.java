package com.example.amirthapa.internproject.Homepage;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;

import android.view.MotionEvent;
import android.view.View;

import android.view.Window;
import android.widget.DigitalClock;


import android.widget.ImageButton;

import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;




import com.dd.CircularProgressButton;
import com.example.amirthapa.internproject.Leavepage.Leaverequest;
import com.example.amirthapa.internproject.Profilepage.Profile;
import com.example.amirthapa.internproject.R;
import com.example.amirthapa.internproject.Reportpage.Report;

import com.example.amirthapa.internproject.Todopage.Todolist;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private ArrayList<HolidaysArray> holidayshorizontallist;
    private Holidayslistadapter holidaysadapter;

    private ArrayList<Emparray>emparrays;
    private Emphorzadepter emphorzadepter;
    public  DrawerLayout drawer;




    DigitalClock clk;


    public static final Integer[] emppic = { R.drawable.man,
            R.drawable.man, R.drawable.man, R.drawable.man,R.drawable.man};

    public static final String[] datearray = new String[]{"4", "28", "23", "9","30"};
   public static final String[] montharray = new String[]{"jan", "feb", "jan", " march","feb"};




    public static final String[] titles = new String[] { "amir thapa is abest now ",
            "sujan thapa is present now" };

    public static final String[] descriptions = new String[] {
            "days.if you do that it will be good for him. hopping for your positive respose fjakldf ;alkdjf a;sdlk ;aslkdfj ;alskdjf a;alskdjf as;dlkfja s;dlkfja dsklf a;sldkjf a;sdkjf a;sdkjf a;skdjf  kasd f;alksdjf ;asldkjf a;lkdjf a;slkdjf a;sdlkfj a;sdkfj a;sdkjf a;slkdjf a;sldkjf a;sldkjf a;slkdjf a;skdfj",
            "leave for a 3 days.if you do that it will be good for him.hopping for your positive respos"};

    public static final Integer[] images = { R.drawable.cover,
            R.drawable.cover, R.drawable.cover, R.drawable.cover };

    ListView noticelistview;
    List<Notificationdata> rowItems;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.navigation);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header=navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        RelativeLayout navprofile=(RelativeLayout)header.findViewById(R.id.gotoprofile);
        navprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Profile.class);
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);

            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);

            }
        });


        RecyclerView holidays=(RecyclerView) findViewById(R.id.holidayslistview);
        holidayshorizontallist=new ArrayList<HolidaysArray>();
        for (int i = 0; i < datearray.length; i++) {
            holidayshorizontallist.add(new HolidaysArray(datearray[i],
                    montharray[i]));

        }



        holidaysadapter=new Holidayslistadapter(holidayshorizontallist);
        LinearLayoutManager hhorizontalLayoutManagaer
                = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        holidays.setLayoutManager(hhorizontalLayoutManagaer);
        holidays.setAdapter(holidaysadapter);
        holidays.addOnItemTouchListener(new RecyclerTouchListener(this, holidays, new Recyclerviewinterface() {
            @Override
            public void onClick(View view, int position) {
                Holidayspoup();

            }
        }));



        RecyclerView emplist=(RecyclerView) findViewById(R.id.emprlist);
        emparrays=new ArrayList<Emparray>();
        for (int i = 0; i < emppic.length; i++) {
            emparrays.add(new Emparray(emppic[i]));

        }

        emphorzadepter=new Emphorzadepter(emparrays);
        LinearLayoutManager ehorizontalLayoutManagaer
                = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        emplist.setLayoutManager(ehorizontalLayoutManagaer);
        emplist.setAdapter(emphorzadepter);




        rowItems = new ArrayList<Notificationdata>();
        for (int i = 0; i < titles.length; i++) {
            Notificationdata item = new Notificationdata(images[i], titles[i], descriptions[i]);
            rowItems.add(item);
        }

        noticelistview = (ListView) findViewById(R.id.noticelist);
            NotificationListAdpt adapter =new NotificationListAdpt(this,R.layout.fronnoticelist,rowItems);
        noticelistview.setAdapter(adapter);



        final CircularProgressButton circularProgressButton =(CircularProgressButton) findViewById(R.id.checkinbutton);//animation for onclick checkin button

        circularProgressButton.setIndeterminateProgressMode(true);
        circularProgressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (circularProgressButton.getProgress() == 0) {
                    circularProgressButton.setProgress(50);
                } else if (circularProgressButton.getProgress() == 100) {
                    circularProgressButton.setProgress(0);
                } else {
                    circularProgressButton.setProgress(100);
                }
            }


        });




        clk = (DigitalClock) findViewById(R.id.digitalClock1); //for clok
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        TextView tdays=(TextView)findViewById(R.id.days);
        TextView tmonth=(TextView)findViewById(R.id.month);
        TextView tdate=(TextView)findViewById(R.id.date);

        Calendar cal= Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM/d/EEE", Locale.ENGLISH);
        String strdate= sdf.format(cal.getTime());
        String[] values=strdate.split("/",0);

        tmonth.setText(values[0]);
        tdate.setText(values[1]);
        tdays.setText(values[2]);

      Bootomweight();
        Onclick();

    }


    private void Holidayspoup() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.poupholidays);
        TextView close = (TextView) dialog.findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    public  void Onclick(){
        RelativeLayout clickleave =(RelativeLayout)findViewById(R.id.leave) ;  // leavrequest onclick
        clickleave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Leaverequest.class);
                startActivity(intent);
            }
        });

        RelativeLayout clickprofile =(RelativeLayout)findViewById(R.id.profile) ;  // leavrequest onclick
        clickprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Profile.class);
                startActivity(intent);
            }
        });

        final ImageButton emlistbutton= (ImageButton) findViewById(R.id.emplist); //employye list onclick
        emlistbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent=new Intent(MainActivity.this,Employeelist.class);
                startActivity(intent);
            }
        });

        RelativeLayout clicktodo =(RelativeLayout)findViewById(R.id.todo) ;  // leavrequest onclick
        clicktodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Todolist.class);
                startActivity(intent);
            }
        });
        RelativeLayout report =(RelativeLayout)findViewById(R.id.report) ;  // leavrequest onclick
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,Report.class);
                startActivity(intent);
            }
        });
        final TextView holidayslist= (TextView)findViewById(R.id.holidayslist);
        holidayslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Holidaylist.class);
                startActivity(intent);
            }
        });
        final TextView noticelist= (TextView)findViewById(R.id.viewnotice);
        noticelist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Notification.class);
                startActivity(intent);
            }
        });

    }

    public  void Bootomweight(){
        Display display =  getWindowManager().getDefaultDisplay(); // for height of the bottom menu bar
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int screen_height = outMetrics.heightPixels;
        View bottom = findViewById(R.id.bottom);
        bottom.getLayoutParams().height = screen_height * 1/10;
    }

    public static interface Recyclerviewinterface{
        public void onClick(View view,int position);
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mainlayout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        }
        else if (id == R.id.mnotification) {
            Intent intent=new Intent(getApplicationContext(),Notification.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);


    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.notices) {
            Intent intent = new Intent(getApplicationContext(), Notification.class);
            startActivity(intent);
        }

            else if(id == R.id.holidays) {
                Intent intent = new Intent(getApplicationContext(), Holidaylist.class);
            startActivity(intent);

            }
        else if(id == R.id.colleagues) {
            Intent intent = new Intent(getApplicationContext(), Colleagues.class);
           startActivity(intent);

        }
        drawer.closeDrawer(GravityCompat.START);


        return true;

    }
    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private Recyclerviewinterface clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final Recyclerviewinterface clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }


            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
