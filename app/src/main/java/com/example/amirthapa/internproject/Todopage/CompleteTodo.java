package com.example.amirthapa.internproject.Todopage;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;

public class CompleteTodo extends AppCompatActivity {


    String title;

public static String ID="id";
    TodoCompleteAdpter completeAdapter;
    ListView completelist;
    TodoCompleteDatabase completehelper;
    ArrayList<ToDoCompletemodel> completetoDoContents=new ArrayList<>();
    private final String TAG=CompleteTodo.class.getSimpleName();
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_todo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        ImageView delbutton=(ImageView) findViewById(R.id.ccdelete);

        completelist= (ListView) findViewById(R.id.completetodolist);


       id=getIntent().getExtras().getInt(ID);
//        title=getIntent().getExtras().getString(TITLE);
        Log.d("the titil is from todo",":::"+title);
        Log.d("the id is from todo",":::"+id);
        completehelper=new TodoCompleteDatabase(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();

        completetoDoContents=completehelper.getcompleteDataFromDB(id);
        for(int i=0;i<completetoDoContents.size();i++){
            Log.d(TAG,"todoComplet id"+completetoDoContents.get(i).getId());
            Log.d(TAG,"todocomplete Title"+completetoDoContents.get(i).getTodoCompleteTitle());
        }
        completeAdapter=new TodoCompleteAdpter(this,R.layout.itemrow_content,completetoDoContents);
        completelist.setAdapter(completeAdapter);
//        completelist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

//        delbutton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SparseBooleanArray checked = completelist.getCheckedItemPositions();
//                for (int i = 0; i < completelist.getCount(); i++){
//
//                    if (checked.get(i)==true)
//                    {
//                        completetoDoContents.remove(i);
//
//                    }
//                    completeAdapter.notifyDataSetChanged();
//
//                }
//
//            }
//        });

    }

}
