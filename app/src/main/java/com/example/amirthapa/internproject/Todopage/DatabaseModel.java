package com.example.amirthapa.internproject.Todopage;

/**
 * Created by PRABHU on 11/12/2015.
 */
public class DatabaseModel {
private String todoTitle;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }
}