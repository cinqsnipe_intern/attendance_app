package com.example.amirthapa.internproject.Leavepage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by amir thapa on 3/8/2017.
 */

public class PendingDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="pendinglisttable";
    private static final int DATABASE_VERSION = 1;
    private static final String PENDINGLIST_TABLE = "create table pendinglist_table(id INTEGER PRIMARY KEY AUTOINCREMENT,pending_title TEXT,pending_description TEXT)";


    Context context;

    public PendingDatabase (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase pendingdb) {
        pendingdb.execSQL(PENDINGLIST_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase pendingdb, int oldVersion, int newVersion) {
        pendingdb.execSQL("DROP TABLE IF EXISTS " +"pendinglist_table");
//
        onCreate(pendingdb);

    }

    public void insertIntopendingDB(String title,String description){

        Log.d("insert", "before insert");
        SQLiteDatabase pendingdb = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("pending_title", title);
        values.put("pending_description",description);
//        values.put("pending_date",date);
        //values.put(,);
        pendingdb.insert("pendinglist_table", null, values);
        pendingdb.close();
        Toast.makeText(context, "insert value", Toast.LENGTH_LONG);
        Log.d("the title is","::"+title);
        Log.d("description is","::"+description);
        Log.d("insert into DB", "After insert");
    }


    public ArrayList<PendingModelclass> getDataFrompendingDB(){
        ArrayList<PendingModelclass> modelList = new ArrayList<PendingModelclass>();
        String query = "select * from pendinglist_table";
        Log.d("all list is",query);
        SQLiteDatabase pendingdb = this.getWritableDatabase();
        Cursor cursor = pendingdb.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                PendingModelclass model = new PendingModelclass();
                model.setId(cursor.getInt(0));
                Log.d("the id is",":"+cursor.getInt(0));
                //Log.d("the id is", String.valueOf(cursor.getColumnIndex("id")));
                model.setPendingtitle(cursor.getString(1));
                Log.d("the title is",cursor.getString(1));
                model.setPendingdescripion(cursor.getString(2));
                Log.d("the decription is",cursor.getString(2));
//                model.setPendingdate(cursor.getString(3));

                // model.setEmail(cursor.getString(1));
                //model.setRoll(cursor.getString(2));
                //model.setAddress(cursor.getString(3));
                //model.setBranch(cursor.getString(4));

                modelList.add(model);
            }while (cursor.moveToNext());
        }


        Log.d("pending data", modelList.toString());


        return modelList;
    }


     /*delete a row from database*/

    public void pendingdeleteARow(String id){
        SQLiteDatabase pendingdb= this.getWritableDatabase();
        pendingdb.delete("pendinglist_table" ,"pending_title"  + "= ?",new String[] {String.valueOf(id)});
//        pendingdb.delete("pendinglist_table" , "pending_title" , "pending_description" + " = ?", new String[] {String.valueOf(id)});
        pendingdb.close();
    }
}



