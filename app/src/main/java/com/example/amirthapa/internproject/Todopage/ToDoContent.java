package com.example.amirthapa.internproject.Todopage;

/**
 * Created by amir thapa on 3/3/2017.
 */

public class ToDoContent {

    String todoContentTitle;
private     int id;
    boolean checked;
    int todoListId;

    public int getTodoListId() {
        return todoListId;
    }

    public void setTodoListId(int todoListId) {
        this.todoListId = todoListId;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getTodoContentTitle() {
        return todoContentTitle;
    }

    public void setTodoContentTitle(String todoContentTitle) {
        this.todoContentTitle = todoContentTitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
