package com.example.amirthapa.internproject.Todopage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amir thapa on 3/7/2017.
 */

public class TodoCompleteAdpter extends ArrayAdapter<ToDoCompletemodel> {


    List<ToDoCompletemodel> todocomplete;
    Context conntext;
    TodoCompleteDatabase databseHelper;

    public TodoCompleteAdpter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public TodoCompleteAdpter(Context context, int resource, ArrayList<ToDoCompletemodel> items) {
        super(context, resource, items);
        this.todocomplete = items;
        this.conntext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.completelistrow, null);
        }

        final ToDoCompletemodel p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.comtext);
            notifyDataSetChanged();



            if (tt1 != null) {
                tt1.setText(p.getTodoCompleteTitle());
            }

        }


        return v;
    }
}

