package com.example.amirthapa.internproject.Todopage;

/**
 * Created by amir thapa on 3/7/2017.
 */

public class ToDoCompletemodel {
    String todoCompleteTitle;
    int id;
    boolean checked;
    int todocontent_id;

    public int getTodocontent_id() {
        return todocontent_id;
    }

    public void setTodocontent_id(int todocontent_id) {
        this.todocontent_id = todocontent_id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getTodoCompleteTitle() {
        return todoCompleteTitle;
    }

    public void setTodoCompleteTitle(String todoCompleteTitle) {
        this.todoCompleteTitle = todoCompleteTitle;
    }
    //    public String getTodocompleteTitle() {
//        return todoCompleteTitle;
//    }
//
//    public void getTodocompleteTitle(String todoCompleteTitle) {
//        this.todoCompleteTitle = todoCompleteTitle;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}

