package com.example.amirthapa.internproject.Homepage;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by amir thapa on 2/8/2017.
 */

public class EmlistViewpageadepter extends android.support.v4.app.FragmentStatePagerAdapter{

    int mNumOfTabs;

    public EmlistViewpageadepter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Present present = new Present();
                return present;
            case 1:
                Absent absent = new Absent();
                return absent;




            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
