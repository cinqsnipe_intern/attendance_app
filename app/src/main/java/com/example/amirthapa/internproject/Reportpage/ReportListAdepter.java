package com.example.amirthapa.internproject.Reportpage;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.List;

/**
 * Created by amir thapa on 3/22/2017.
 */

public class ReportListAdepter extends ArrayAdapter<Reportlistmodel> {
    Context context;

    public ReportListAdepter(Context context, int resourceId ,List<Reportlistmodel>items) {
        super(context, resourceId,items);
        this.context=context;
    }


private class ViewHolder{
    TextView reportdate;
    TextView chekindate;
    TextView checkoutdate;
    TextView duration;
}



public  View getView (int position,View convertView,ViewGroup parent){
    ReportListAdepter.ViewHolder holder= null;
    Reportlistmodel reportlistmodel= getItem(position);

    LayoutInflater mInflater = (LayoutInflater) context
            .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);


        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.reportlistrow, null);
            holder = new ReportListAdepter.ViewHolder();
         holder.reportdate=(TextView)convertView.findViewById(R.id.rptdate) ;
            holder.chekindate = (TextView) convertView.findViewById(R.id.checkintime);
            holder.checkoutdate = (TextView) convertView.findViewById(R.id.cheouttime);
            holder.duration = (TextView) convertView.findViewById(R.id.duration);
            convertView.setTag(holder);
        }
        else
            holder = (ReportListAdepter.ViewHolder) convertView.getTag();
    holder.reportdate.setText(reportlistmodel.getRptdate());
    holder.chekindate.setText(reportlistmodel.getCheckin());
    holder.checkoutdate.setText(reportlistmodel.getCheckout());
     holder.duration.setText(reportlistmodel.getRptduration());



       return  convertView;
    }


    }
