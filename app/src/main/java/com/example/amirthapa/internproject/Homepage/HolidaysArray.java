package com.example.amirthapa.internproject.Homepage;

/**
 * Created by amir thapa on 2/26/2017.
 */

public class HolidaysArray {

        String date;
        String month;

        public HolidaysArray(String date, String month) {
            this.date = date;
            this.month=month;
        }
        public String getDate() {
            return date;
        }
        public String getMonth() {
            return month;
        }}

