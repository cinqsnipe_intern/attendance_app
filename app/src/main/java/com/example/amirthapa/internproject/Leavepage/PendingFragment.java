package com.example.amirthapa.internproject.Leavepage;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PendingFragment extends Fragment {

//    ArrayList<PendingModelclass> rowitems;
//    LeavesRowitems rowitem;
    ArrayList<String> list = new ArrayList<String>();
    //  ArrayAdapter<String> adapter;
     PendingAdepter pendingAdepter;
    PendingDatabase pendinghelpher;
    ArrayList<PendingModelclass> pendingArrayList = new ArrayList<>();
//    String[] title={"sickness","homework","personalwork"};
//    String[] date={"jan 14-jan 16","jan 14-jan 16","jan 14-jan 16"};
//    String[] description={"Register your mobile to eSewa sljfg sg asdjf a;slkdjf as;dkfja a;slkdjf a;lskdfj a;sldkfj as;doei a;sdlkfja eo a;sdkf 'dfjg asdj as;ldj a","Know About eSewa falksdjfa alskdjf a;sldkfja sd;flk" ,"Know  alksdjf a;lskdjf a;sldkf a;skdlfj asdfAbout Fonepay" };
    ListView plistView;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pending, container, false);


        plistView=(ListView) v.findViewById(R.id.listviewp);
        pendinghelpher = new PendingDatabase(getContext());
        final PendingModelclass pendingModelclass=new PendingModelclass();
        pendingArrayList = pendinghelpher.getDataFrompendingDB();
        pendingAdepter = new PendingAdepter(getContext(), R.layout.leavelist, pendingArrayList);
        plistView.setAdapter(pendingAdepter);

        pendingAdepter.add(pendingModelclass);
        pendingAdepter.notifyDataSetChanged();

        plistView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final Dialog dialogdelete = new Dialog(getActivity());

                dialogdelete.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogdelete.setContentView(R.layout.listdeletedilog);
                TextView cancledelete = (TextView) dialogdelete.findViewById(R.id.deletecancle);
                TextView ldelete = (TextView) dialogdelete.findViewById(R.id.delete);
                cancledelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogdelete.dismiss();
                    }
                });
                ldelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PendingModelclass pendingModelclass=pendingArrayList.get(position);
                        pendinghelpher.pendingdeleteARow(pendingArrayList.get(position).getPendingtitle());
                        //todolistArrayList.remove(position);
                        pendingAdepter.remove(pendingModelclass);
                        //Intent intent=new Intent(Todolist.this,Todolist.class);
                        //startActivity(intent);
                        dialogdelete.dismiss();

                    }
                });
                dialogdelete.show();


                return true;
            }
        });


//        rowitems = new ArrayList<>();
//        for (int i=0;i<title.length;i++)
//        {
//            rowitem=new LeavesRowitems();
//
//            rowitem.setTitle(title[i]);
//            rowitem.setDate(date[i]);
//            rowitem.setDescription(description[i]);
//            rowitems.add(rowitem);
//
//            LeaveBaseAdapter leaveBaseAdapter= new LeaveBaseAdapter(getContext(),rowitems);
//            plistView.setAdapter(leaveBaseAdapter);
//
//        }
        return (v);
    }

}
