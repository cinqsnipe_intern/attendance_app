package com.example.amirthapa.internproject;

/**
 * Created by amir thapa on 3/7/2017.
 */

public class AndroidVerson {


    private  String title;
    private  String description;
    private String image;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
