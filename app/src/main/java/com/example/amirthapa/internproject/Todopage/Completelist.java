package com.example.amirthapa.internproject.Todopage;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Completelist extends Fragment {


    TodoCompleteAdpter completeAdapter;
    ListView completelist;
    TodoCompleteDatabase completehelper;
    ArrayList<ToDoCompletemodel> completetoDoContents=new ArrayList<>();
   private int cid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /** Inflating the layout for this fragment **/
        View v = inflater.inflate(R.layout.fragment_completelist, container, false);
         cid = getArguments().getInt("ID");
        completelist= (ListView) v.findViewById(R.id.completetodolist);
        completehelper=new TodoCompleteDatabase(getContext());
        completetoDoContents=completehelper.getcompleteDataFromDB(cid);
        completeAdapter=new TodoCompleteAdpter(getActivity(),R.layout.completelistrow,completetoDoContents);
        completelist.setAdapter(completeAdapter);
        completelist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, final long id) {
                final Dialog dialogdelete = new Dialog(getContext());

                dialogdelete.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogdelete.setContentView(R.layout.listdeletedilog);
                final TextView cancledelete = (TextView) dialogdelete.findViewById(R.id.deletecancle);
               final int idd=completetoDoContents.get(position).getId();
                TextView ldelete = (TextView) dialogdelete.findViewById(R.id.delete);
                cancledelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogdelete.dismiss();
                    }
                });
                ldelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ToDoCompletemodel databasemodelc = completetoDoContents.get(position);
//                DatabaseModel databaseModel=ctoDoContents.get(position);

                        completehelper.DeleteComplete(idd);
                        completeAdapter.remove(databasemodelc);
                        completeAdapter.notifyDataSetChanged();
                        dialogdelete.dismiss();



                    }
                });
                dialogdelete.show();


                return true;
            }
        });


        return (v);
    }


    }


