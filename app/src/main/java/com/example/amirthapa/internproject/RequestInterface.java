package com.example.amirthapa.internproject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by amir thapa on 3/7/2017.
 */

public interface RequestInterface {
    @GET("api/notice")
    Call<JSONResponse> getJSON();
}
