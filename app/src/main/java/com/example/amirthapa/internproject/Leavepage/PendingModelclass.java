package com.example.amirthapa.internproject.Leavepage;

import java.sql.Date;

/**
 * Created by amir thapa on 3/8/2017.
 */

public class PendingModelclass {

    private String pendingtitle;
    private String pendingdescripion;
    private String pendingdate;
    private int id;


    public String getPendingdate() {
        return pendingdate;
    }

    public void setPendingdate(String pendingdate) {
        this.pendingdate = pendingdate;
    }



    public String getPendingtitle() {
        return pendingtitle;
    }

    public void setPendingtitle(String pendingtitle) {
        this.pendingtitle = pendingtitle;
    }

    public String getPendingdescripion() {
        return pendingdescripion;
    }

    public void setPendingdescripion(String pendingdescripion) {
        this.pendingdescripion = pendingdescripion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
