package com.example.amirthapa.internproject.Homepage;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.List;

/**
 * Created by amir thapa on 2/8/2017.
 */

public class EmployeelistAdepter extends ArrayAdapter<Employeedata> {

    Context context;

    public EmployeelistAdepter(Context context, int resourceId,
                                 List<Employeedata> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        RoundedImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
         Employeedata employeedata = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.employeelist, null);
            holder = new ViewHolder();
            holder.txtDesc = (TextView) convertView.findViewById(R.id.emppost);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.empname);
            holder.imageView = (RoundedImageView) convertView.findViewById(R.id.empimage);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txtDesc.setText(employeedata.getDesc());
        holder.txtTitle.setText(employeedata.getTitle());
        holder.imageView.setImageResource(employeedata.getImageId());

        return convertView;
    }


}

