package com.example.amirthapa.internproject.Leavepage;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.amirthapa.internproject.Homepage.MainActivity;
import com.example.amirthapa.internproject.Homepage.Notification;
import com.example.amirthapa.internproject.Profilepage.Profile;
import com.example.amirthapa.internproject.R;
import com.example.amirthapa.internproject.Reportpage.Report;
import com.example.amirthapa.internproject.Todopage.Todolist;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.playlog.internal.LogEvent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Leaverequest extends AppCompatActivity implements View.OnClickListener {


    private EditText fromDateEtxt;
    private EditText toDateEtxt;
    private EditText title;
    private EditText description;
    private TextView submitrequest;
    private  TextView pendingdate;
    public String fromdate;



    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaverequest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        title=(EditText)findViewById(R.id.reason);
        description=(EditText)findViewById(R.id.pdescription);
        pendingdate=(TextView)findViewById(R.id.leavedate);
        submitrequest=(TextView) findViewById(R.id.submitleave);
        final PendingDatabase pendinghelper=new PendingDatabase(Leaverequest.this);

        submitrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pendingtitlevalue=title.getText().toString();
                String pendingdescriptionvalue=description.getText().toString();
                if (pendingdescriptionvalue.isEmpty() || pendingtitlevalue.isEmpty()){
                    Toast.makeText(Leaverequest.this, "leave request or decription is emply", Toast.LENGTH_SHORT).show();
                }
                else {

                    PendingModelclass pendingmodelclass = new PendingModelclass();
                    pendingmodelclass.setPendingtitle(pendingtitlevalue);
                    pendingmodelclass.setPendingdescripion(pendingdescriptionvalue);
                    pendinghelper.insertIntopendingDB(pendingtitlevalue, pendingdescriptionvalue);
                    Toast.makeText(Leaverequest.this, "sucessfully added", Toast.LENGTH_SHORT).show();
                    finish();
                    overridePendingTransition( 0, 0);
                    startActivity(getIntent());
                    overridePendingTransition( 0, 0);
                }

            }
        });



        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager); //setting view pager
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);  //for tablayout
        tabLayout.setTabTextColors(Color.parseColor("#E7E7E7"), Color.parseColor("#0077c0"));
        LeaveViewpagerAdepter leaveViewpagerAdepter=new LeaveViewpagerAdepter(getSupportFragmentManager());
        viewPager.setAdapter(leaveViewpagerAdepter);
        viewPager.setCurrentItem(1);
        tabLayout.setupWithViewPager(viewPager);







        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);  //using digital clock

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        fab.hide();
        Bootomweight();
        Onclickbottombar();
        findViewsById();
        setDateTimeField();

    }

    private void findViewsById() {
        fromDateEtxt = (EditText) findViewById(R.id.startfrom);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();

        toDateEtxt = (EditText) findViewById(R.id.endto);
        toDateEtxt.setInputType(InputType.TYPE_NULL);


    }

    private void setDateTimeField() {
        fromDateEtxt.setOnClickListener(this);
        toDateEtxt.setOnClickListener(this);

        final Calendar newCalendar = Calendar.getInstance();

        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newfDate = Calendar.getInstance();
               newfDate.set(year, monthOfYear, dayOfMonth);
              fromdate=dateFormatter.format(newfDate.getTime()).toString();

              if(newfDate.compareTo(Calendar.getInstance())>=0){

                  fromDateEtxt.setError(null);
                  fromDateEtxt.setText(fromdate);

              }
              else {
                  fromDateEtxt.setError("you can't take leave from past");
              }




            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();

                newDate.set(year, monthOfYear, dayOfMonth);
                String todate=dateFormatter.format(newDate.getTime()).toString();

if(!TextUtils.isEmpty(fromdate)) {
    if (fromdate.compareTo(todate) <= 0) {
        toDateEtxt.setError(null);
        toDateEtxt.setText(todate);

    } else {
        toDateEtxt.setError("invalid date");
    }
}else{
    toDateEtxt.setError("please select startdate first");
}




                                        }




        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View view) {
        if (view == fromDateEtxt ) {
            fromDatePickerDialog.show();
        } else if (view == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }
    @Override
    public  void onBackPressed(){

        super.onBackPressed();
        finish();
    }
    public  void Bootomweight(){
        Display display =  getWindowManager().getDefaultDisplay(); // for height of the bottom menu bar
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int screen_height = outMetrics.heightPixels;
        View bottom = findViewById(R.id.bottom);
        bottom.getLayoutParams().height = screen_height * 1/10;
    }
    public  void Onclickbottombar(){
        RelativeLayout clickprofile =(RelativeLayout)findViewById(R.id.profile) ;  // leavrequest onclick
        clickprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Leaverequest.this,Profile.class);
                startActivity(intent);
            }
        });

        RelativeLayout clicktodo =(RelativeLayout)findViewById(R.id.todo) ;  // leavrequest onclick
        clicktodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Leaverequest.this,Todolist.class);
                startActivity(intent);
            }
        });

        RelativeLayout report =(RelativeLayout)findViewById(R.id.Reports) ;  // leavrequest onclick
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Leaverequest.this,Report.class);
                startActivity(intent);
            }
        });

        RelativeLayout dashbord = (RelativeLayout) findViewById(R.id.dashbord); //for go to dahsbord

        dashbord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Leaverequest.this, MainActivity.class);
                startActivity(intent);
            }
        });

        TextView viewleavhisty=(TextView)findViewById(R.id.leavhistorylist);
        viewleavhisty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Leaverequest.this,Fullleaverequest.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.leave_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        }
        else if (id == R.id.mnotification) {
            Intent intent=new Intent(getApplicationContext(),Notification.class);
            startActivity(intent);

        }


        return super.onOptionsItemSelected(item);


    }


}
