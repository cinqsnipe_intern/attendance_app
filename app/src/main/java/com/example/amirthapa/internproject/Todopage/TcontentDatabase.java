package com.example.amirthapa.internproject.Todopage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by amir thapa on 3/3/2017.
 */

public class TcontentDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="todocontentlist";
    private static final int DATABASE_VERSION = 1;
    private static final String TODOCONTENT_TABLE = "create table todocontent_table(id INTEGER PRIMARY KEY AUTOINCREMENT,todocontent_title TEXT,todolist_id INTEGER)";

    Context context;

    public TcontentDatabase (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase cdb) {

        cdb.execSQL(TODOCONTENT_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase cdb, int oldVersion, int newVersion) {

        cdb.execSQL("DROP TABLE IF EXISTS " +"todocontent_table");
//
        onCreate(cdb);
    }
    /* Insert into database*/
    public void insertIntoDBcontent(int cid,String ctitle){

        Log.d("insert", "before insert");
        SQLiteDatabase cdb = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("todocontent_title", ctitle);
        values.put("todolist_id",cid);
        cdb.insert("todocontent_table", null, values);
        cdb.close();
        Toast.makeText(context, "insert value", Toast.LENGTH_LONG);
        Log.d("the title is","::"+ctitle);
        Log.d("insert into DB", "After insert");
    }



    public ArrayList<ToDoContent> getcontentDataFromDB(int cid){
        ArrayList<ToDoContent> cmodelList = new ArrayList<ToDoContent>();
        String query = "select * from todocontent_table where todolist_id="+cid;
        Log.d("all list is",query);
        SQLiteDatabase cdb = this.getWritableDatabase();
        Cursor cursor = cdb.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                ToDoContent contentmodel = new ToDoContent();
                contentmodel.setId(cursor.getInt(cursor.getColumnIndex("id")));
                contentmodel.setTodoContentTitle(cursor.getString(cursor.getColumnIndex("todocontent_title")));
                contentmodel.setTodoListId(cursor.getShort(cursor.getColumnIndex("todolist_id")));
                cmodelList.add(contentmodel);
            }while (cursor.moveToNext());
        }


        Log.d("student data", cmodelList.toString());

        return cmodelList;
    }


    /*delete a row from database*/

    public void contentdeleteARow(int cid){
        SQLiteDatabase cdb= this.getWritableDatabase();
        cdb.delete("todocontent_table", "id" + " = ?", new String[] {String.valueOf(cid)});
//       cdb.delete("todocontent_table","todolist_id="+String.valueOf(cid),null);
        Log.d("Succesfully deleted cid","::"+cid);

        cdb.close();
    }



}

