package com.example.amirthapa.internproject.Leavepage;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by amir thapa on 2/6/2017.
 */

public class LeaveViewpagerAdepter extends FragmentPagerAdapter {



    public LeaveViewpagerAdepter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
             fragment = new PendingFragment();
        }
        else if (position == 1)
        {
            fragment = new ApprovedFragment();
        }
        else if (position == 2)
        {
            fragment = new RejectFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Pending";
        }
        else if (position == 1)
        {
            title = "Approved";
        }
        else if (position == 2)
        {
            title = "Reject";
        }
        return title;
    }
}



