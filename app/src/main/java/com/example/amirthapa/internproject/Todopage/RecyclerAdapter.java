package com.example.amirthapa.internproject.Todopage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;
import com.example.amirthapa.internproject.Todopage.DatabaseModel;

import java.util.ArrayList;

/**
 * Created by user_adnig on 11/14/15.
 */
public class RecyclerAdapter extends ArrayAdapter<DatabaseModel> {

public RecyclerAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        }

public RecyclerAdapter(Context context, int resource, ArrayList<DatabaseModel> items) {
        super(context, resource, items);
        }

@Override
public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
        LayoutInflater vi;
        vi = LayoutInflater.from(getContext());
        v = vi.inflate(R.layout.item_row, null);
        }

        DatabaseModel p = getItem(position);

        if (p != null) {
        TextView tt1 = (TextView) v.findViewById(R.id.rvname);



        if (tt1 != null) {
        tt1.setText(p.getTodoTitle());
        }

        }

        return v;
        }

        }