package com.example.amirthapa.internproject.Todopage;


import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.amirthapa.internproject.R;

import java.util.ArrayList;


public class TodoContentlist extends AppCompatActivity {


    public static   String TITLE="title";
    public  static  String ID="id";

    ToDoContentAdapter crecyclerAdapter;
    ListView clist;
    TcontentDatabase chelper;
    ArrayList<ToDoContent> ctoDoContents=new ArrayList<>();
    private final String TAG=TodoContentlist.class.getSimpleName();
public  int ids;
    String contenttitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todocontent);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        clist= (ListView) findViewById(R.id.listtodocontent);

       ids =getIntent().getExtras().getInt(ID);
        contenttitle  =getIntent().getExtras().getString(TITLE);
                    Log.d("the titil is from todo",":::"+contenttitle);
                    Log.d("the id is from todo",":::"+ids);
                    chelper=new TcontentDatabase(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        //comment gareko
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });


        getSupportActionBar().setTitle(contenttitle);

        Toast.makeText(this, contenttitle, Toast.LENGTH_SHORT).show();



        Button addbtn=(Button)findViewById(R.id.addtodo);  //add button of todo content
        final EditText ctitletext=(EditText)findViewById(R.id.todocontentitem);

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String todocontenttitle=ctitletext.getText().toString();

                ToDoContent todocontentmodel = new ToDoContent();
                if (todocontenttitle.equals("")) {
                    Toast.makeText(TodoContentlist.this, "Please fill all the fields", Toast.LENGTH_LONG).show();
                } else {


                    todocontentmodel.setTodoContentTitle(todocontenttitle);
                    todocontentmodel.setTodoListId(ids);
                    chelper.insertIntoDBcontent(ids,todocontenttitle);
                    Log.d("created ids value","::"+ids);
                    crecyclerAdapter.add(todocontentmodel);
                    crecyclerAdapter.notifyDataSetChanged();

                    ctitletext.setText("");

                    onResume();
                }


            }
        });
        clist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, final long id) {
                final Dialog dialogdelete = new Dialog(TodoContentlist.this);

                dialogdelete.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogdelete.setContentView(R.layout.listdeletedilog);
                final TextView cancledelete = (TextView) dialogdelete.findViewById(R.id.deletecancle);
                TextView ldelete = (TextView) dialogdelete.findViewById(R.id.delete);
                cancledelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogdelete.dismiss();
                    }
                });
                ldelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ToDoContent databasemodel2 = ctoDoContents.get(position);
                        int idee=ctoDoContents.get(position).getId();
                        Log.d("value of ids",":::"+ids);
                        chelper.contentdeleteARow(idee);

                        crecyclerAdapter.remove(databasemodel2);
                        crecyclerAdapter.notifyDataSetChanged();
                        dialogdelete.dismiss();



                    }
                });
                dialogdelete.show();


                return true;
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ctoDoContents=chelper.getcontentDataFromDB(ids);
        for(int i=1;i<ctoDoContents.size();i++){
            Log.d(TAG,"todocontent title"+ctoDoContents.get(i).getTodoContentTitle());
            Log.d(TAG,"todocontent id"+ctoDoContents.get(i).getId());
            Log.d(TAG,"todolist id"+ctoDoContents.get(i).getTodoListId());

        }

        crecyclerAdapter=new ToDoContentAdapter(this,R.layout.itemrow_content,ctoDoContents,ids);
        clist.setAdapter(crecyclerAdapter);



        Button completebutton= (Button) findViewById(R.id.completebtn);
        completebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putInt("ID",ids);

                android.support.v4.app.FragmentManager fragmentManager = TodoContentlist.this.getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                Completelist completelistd = new Completelist();

                completelistd.setArguments(bundle);
                fragmentTransaction.replace(R.id.fragment_container,completelistd,"something");
                fragmentTransaction.commit();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_todo_content, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            this.onBackPressed();
           this.finish();
        }
        if(id==R.id.notification_id){
            Toast.makeText(this, "clickd notificatio", Toast.LENGTH_SHORT).show();
        }
            return true;
        }

}
