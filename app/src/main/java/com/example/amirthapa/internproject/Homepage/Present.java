package com.example.amirthapa.internproject.Homepage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Present extends Fragment {


    public static final String[] titles = new String[] { "amir thapa",
            "sujan thapa", "ram thapa", "shyam thapa" };

    public static final String[] descriptions = new String[] {
            "android",
            "ios", "designer",
            "ceo" };

    public static final Integer[] images = { R.drawable.man,
            R.drawable.man, R.drawable.man, R.drawable.man };

    ListView plistView;
    List<Employeedata> rowItems;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_present, container, false);




        rowItems = new ArrayList<Employeedata>();
        for (int i = 0; i < titles.length; i++) {
            Employeedata item = new Employeedata(images[i], titles[i], descriptions[i]);
            rowItems.add(item);
        }

        plistView = (ListView) v.findViewById(R.id.plist);
        EmployeelistAdepter adapter = new EmployeelistAdepter(getContext(),
                R.layout.employeelist, rowItems);
        plistView.setAdapter(adapter);



        return (v);
    }


}
