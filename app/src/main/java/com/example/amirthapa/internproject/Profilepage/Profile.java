package com.example.amirthapa.internproject.Profilepage;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.amirthapa.internproject.Homepage.MainActivity;
import com.example.amirthapa.internproject.Homepage.Notification;
import com.example.amirthapa.internproject.Leavepage.Leaverequest;
import com.example.amirthapa.internproject.R;
import com.example.amirthapa.internproject.Reportpage.Report;
import com.example.amirthapa.internproject.Todopage.Todolist;

public class Profile extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout clickdashbord;
    private  RelativeLayout clicktodo;
    private RelativeLayout clickleave;
    private RelativeLayout clickreport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide(); //hide fab button
        Bootomweight(); //to provide a weight of button

        clickdashbord =(RelativeLayout)findViewById(R.id.dashbord) ;  // home onclick
        clickdashbord.setOnClickListener(this);

        clicktodo =(RelativeLayout)findViewById(R.id.todo) ;  // todolist onclick
        clicktodo.setOnClickListener(this);

     clickleave =(RelativeLayout)findViewById(R.id.leave) ;  //  leave  onclick
        clickleave.setOnClickListener(this);

        clickreport=(RelativeLayout)findViewById(R.id.Reports);
        clickreport.setOnClickListener(this);



    }

    public  void Bootomweight(){
        Display display =  getWindowManager().getDefaultDisplay(); // for height of the bottom menu bar
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int screen_height = outMetrics.heightPixels;
        View bottom = findViewById(R.id.bottom);
        bottom.getLayoutParams().height = screen_height * 1/10;
    }

    @Override
    public void onClick(View v) {
        if (v == clickdashbord) {
            Intent intent=new Intent(Profile.this,MainActivity.class);
            startActivity(intent);

        } else if (v == clicktodo) {
            Intent intent=new Intent(Profile.this,Todolist.class);
            startActivity(intent);

        }
        else if (v== clickleave) {
            Intent intent=new Intent(Profile.this,Leaverequest.class);
            startActivity(intent);

        }
        else if (v == clickreport) {
            Intent intent=new Intent(Profile.this,Report.class);
            startActivity(intent);

        }
    }


    @Override
    public  void onBackPressed(){

        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        }
        else if (id == R.id.pnotification) {
            Intent intent=new Intent(getApplicationContext(),Notification.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);


    }


}
