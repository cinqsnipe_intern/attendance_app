package com.example.amirthapa.internproject.Reportpage;

/**
 * Created by amir thapa on 3/22/2017.
 */

public class Reportlistmodel {
    private String rptdate;
    private String checkin;
    private String checkout;
    private String rptduration;

    public Reportlistmodel(String rptdate, String checkin, String checkout,String rptduration) {

       this.rptdate=rptdate;
        this.checkin=checkin;
        this.checkout=checkout;
        this.rptduration=rptduration;
    }

    public String getRptdate() {
        return rptdate;
    }

    public void setRptdate(String rptdate) {
        this.rptdate = rptdate;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getRptduration() {
        return rptduration;
    }

    public void setRptduration(String rptduration) {
        this.rptduration = rptduration;
    }

    @Override
    public String toString() {
        return rptdate + "\n" + checkin +"\n" + checkout +"\n" + rptduration;
    }
}
