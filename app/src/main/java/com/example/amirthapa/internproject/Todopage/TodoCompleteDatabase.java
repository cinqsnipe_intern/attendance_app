package com.example.amirthapa.internproject.Todopage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by amir thapa on 3/7/2017.
 */

public class TodoCompleteDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="todocomplete";
    private static final int DATABASE_VERSION = 1;
    private static final String TODOCOMPLETE_TABLE = "create table todocomplete_table(id INTEGER PRIMARY KEY AUTOINCREMENT,todocomplete_title TEXT ,todocontent_id INTEGER)";



    Context context;

    public TodoCompleteDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase completedb) {
        completedb.execSQL(TODOCOMPLETE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase completedb, int oldVersion, int newVersion) {

        completedb.execSQL("DROP TABLE IF EXISTS " +"todocomplete_table");
        onCreate(completedb);

    }
    public void insertIntocCompleteDB(int cmid,String completetitle){
        Log.d("insert", "before insert");
        SQLiteDatabase completedb = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("todocomplete_title", completetitle);
        values.put("todocontent_id",cmid);

        completedb.insert("todocomplete_table", null, values);
        completedb.close();
        Toast.makeText(context, "insert value", Toast.LENGTH_LONG);
        Log.d("the title is","::"+completetitle);
        Log.d("insert into DB", "After insert");
    }
    public ArrayList<ToDoCompletemodel> getcompleteDataFromDB(int cmid){
        ArrayList<ToDoCompletemodel> cmodelList = new ArrayList<ToDoCompletemodel>();
        String query = "select * from todocomplete_table where todocontent_id="+cmid;
        Log.d("all list is",query);
        SQLiteDatabase cdb = this.getWritableDatabase();
        Cursor cursor = cdb.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                ToDoCompletemodel contentmodel = new ToDoCompletemodel();
                contentmodel.setId(cursor.getInt(cursor.getColumnIndex("id")));
                contentmodel.setTodoCompleteTitle(cursor.getString(cursor.getColumnIndex("todocomplete_title")));
                contentmodel.setTodocontent_id(cursor.getInt(cursor.getColumnIndex("todocontent_id")));

                cmodelList.add(contentmodel);
            }while (cursor.moveToNext());
        }


        Log.d("student data", cmodelList.toString());


        return cmodelList;


    }

    public void DeleteComplete(int cmid){
        SQLiteDatabase cdb= this.getWritableDatabase();
        cdb.delete("todocomplete_table", "id" + " = ?", new String[] {String.valueOf(cmid)});
        Log.d("Succesfully deleted ct","::"+cmid);
        cdb.close();
    }




}
