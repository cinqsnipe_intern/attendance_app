package com.example.amirthapa.internproject.Homepage;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;


import com.example.amirthapa.internproject.R;

import java.util.ArrayList;


public class Holidaylist extends AppCompatActivity {
    private ArrayList<HolidaysArray> holidayshorizontallist;
    private Holidayslistadapter holidaysadapter;
    public static final String[] datearray = new String[]{"4", "28", "23", "9","30"};
    public static final String[] montharray = new String[]{"jan", "feb", "jan", " march","feb"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holidaylist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        RecyclerView holidayslist=(RecyclerView) findViewById(R.id.holidaysfulllistview);
        holidayshorizontallist=new ArrayList<HolidaysArray>();
        for (int i = 0; i < datearray.length; i++) {
            holidayshorizontallist.add(new HolidaysArray(datearray[i],
                    montharray[i]));

        }




        holidaysadapter=new Holidayslistadapter(holidayshorizontallist);
        LinearLayoutManager hhorizontalLayoutManagaer
                = new LinearLayoutManager(Holidaylist.this, LinearLayoutManager.HORIZONTAL, false);
        holidayslist.setLayoutManager(hhorizontalLayoutManagaer);
        holidayslist.setAdapter(holidaysadapter);






        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();





//        CalendarView simpleCalendarView = (CalendarView) findViewById(R.id.simpleCalendarView); // get the reference of CalendarView

    }

    private void Showdilog() {
        final Dialog dialog = new Dialog(Holidaylist.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.poupholidays);
        TextView close = (TextView) dialog.findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }
    @Override
    public  void onBackPressed(){

        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.holidays_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        } else if (id == R.id.hnotification) {
            Intent intent = new Intent(getApplicationContext(), Notification.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);


    }
}