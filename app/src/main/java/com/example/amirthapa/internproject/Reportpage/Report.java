package com.example.amirthapa.internproject.Reportpage;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Report extends AppCompatActivity {
    List<Reportlistmodel>reportlists;

    public static final String [] reportdate={"march-28","march-28","march-28","march-28","march-28"};
    public static final String [] checkindate={"10:30 pm","10:30 pm","10:30 pm","m10:30 pm","10:30 pm"};
    public static final String [] checkoutdate={"10:30 pm","10:30 pm","10:30 pm","m10:30 pm","10:30 pm"};
    public static final String [] duration={"10:30 pm","10:30 pm","10:30 pm","m10:30 pm","10:30 pm"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        //comment gareko
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        reportlists =new ArrayList<Reportlistmodel>();
                for (int i = 0; i < reportdate.length; i++) {
                    Reportlistmodel reportlistmodel=new Reportlistmodel(reportdate[i],checkindate[i],checkoutdate[i],duration[i]);

            reportlists.add(reportlistmodel);
        }

        ListView reportlistview=(ListView)findViewById(R.id.reportlist);
        ReportListAdepter reportListAdepter=new ReportListAdepter(this,R.layout.reportlistrow,reportlists);
        reportlistview.setAdapter(reportListAdepter);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
        Date date = new Date();
        TextView rpttitle=(TextView)findViewById(R.id.rptmonth);
        rpttitle.setText(dateFormat.format(date).toString());






        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();


        Display display = getWindowManager().getDefaultDisplay();  //giving weight to bootm menue
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int screen_height = outMetrics.heightPixels;
        View bottom = findViewById(R.id.bottom);
        bottom.getLayoutParams().height = screen_height * 1 / 10;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
}
