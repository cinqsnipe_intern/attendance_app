package com.example.amirthapa.internproject.Homepage;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;
import java.util.List;

public class Colleagues extends AppCompatActivity {

    public static final String[] titles = new String[] { "amir thapa",
            "sujan thapa", "ram thapa", "shyam thapa" , "shyam thapa" , "shyam thapa" , "shyam thapa" };

    public static final String[] descriptions = new String[] {
            "android",
            "ios", "designer",
            "ceo","android","android","android" };

    public static final Integer[] images = { R.drawable.man,
            R.drawable.man, R.drawable.man, R.drawable.man, R.drawable.man, R.drawable.man, R.drawable.man };

    ListView emplistView;
    List<Employeedata> rowItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colleagues);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        //comment gareko
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.backbutton);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();


        rowItems = new ArrayList<Employeedata>();
        for (int i = 0; i < titles.length; i++) {
            Employeedata item = new Employeedata(images[i], titles[i], descriptions[i]);
            rowItems.add(item);
        }

        emplistView = (ListView) findViewById(R.id.listviewallemp);
        EmployeelistAdepter adapter = new EmployeelistAdepter(this,
                R.layout.employeelist, rowItems);
        emplistView.setAdapter(adapter);
    }

    @Override
    public  void onBackPressed(){

        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.collegues_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

        } else if (id == R.id.colznotification) {
            Intent intent = new Intent(getApplicationContext(), Notification.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);


    }

}
