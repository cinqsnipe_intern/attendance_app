package com.example.amirthapa.internproject.Todopage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.amirthapa.internproject.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amir thapa on 3/3/2017.
 */

public class ToDoContentAdapter extends ArrayAdapter<ToDoContent> {


    List<ToDoContent> toDoContents;
    Context conntext;
//    int id;
    TodoCompleteDatabase databseHelper;
    TcontentDatabase database;
    int id;
    public ToDoContentAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ToDoContentAdapter(Context context, int resource, ArrayList<ToDoContent> items,int id) {
        super(context, resource, items);
        this.toDoContents=items;
        this.conntext=context;
        this.id=id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.itemrow_content, null);
        }

        final ToDoContent p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.crvname);
            CheckBox cBox= (CheckBox) v.findViewById(R.id.checkbox);
            databseHelper=new TodoCompleteDatabase(conntext);
            cBox.setChecked(toDoContents.get(position).isChecked());
            cBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked) {

                        String title = toDoContents.get(position).getTodoContentTitle();
                         int idee=toDoContents.get(position).getId();
                        databseHelper.insertIntocCompleteDB(id, title);
                        new TcontentDatabase(conntext).contentdeleteARow(idee);
                        toDoContents.remove(p);
                        notifyDataSetChanged();

                    }



                }
            });



            if (tt1 != null) {
                tt1.setText(p.getTodoContentTitle());
            }

        }


        return v;
    }



}
