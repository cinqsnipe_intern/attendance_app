package com.example.amirthapa.internproject.Todopage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by PRABHU on 11/12/2015.
 */
public class DatabaseHelpher extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="todolist";
    private static final int DATABASE_VERSION = 1;
    private static final String TODO_TABLE = "create table todo_table (id INTEGER PRIMARY KEY AUTOINCREMENT,todo_title TEXT)";

    Context context;

    public DatabaseHelpher(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TODO_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TODO_TABLE);
        onCreate(db);
    }
/* Insert into database*/
    public void insertIntoDB(String title){

        Log.d("insert", "before insert");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("todo_title", title);
        //values.put(,);
        db.insert("todo_table", null, values);
        db.close();
        Toast.makeText(context, "insert value", Toast.LENGTH_LONG);
        Log.d("the title is","::"+title);
        Log.d("insert into DB", "After insert");
    }



    public ArrayList<DatabaseModel> getDataFromDB(){
        ArrayList<DatabaseModel> modelList = new ArrayList<DatabaseModel>();
//        String query = "select * from todo_table";
//    Log.d("all list is",query);

        SQLiteDatabase db = this.getReadableDatabase();
Cursor cursor = db.rawQuery("SELECT * FROM todo_table ",null);
        if(cursor.getCount()!=0) {

            if (cursor.moveToFirst()) {
                do {
                    DatabaseModel model = new DatabaseModel();
                    model.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    Log.d("the id is", ":" + cursor.getInt(cursor.getColumnIndex("id")));
                    //Log.d("the id is", String.valueOf(cursor.getColumnIndex("id")));
                    model.setTodoTitle(cursor.getString(cursor.getColumnIndex("todo_title")));
                    Log.d("the title is", cursor.getString(cursor.getColumnIndex("todo_title")));
                    // model.setEmail(cursor.getString(1));
                    //model.setRoll(cursor.getString(2));
                    //model.setAddress(cursor.getString(3));
                    //model.setBranch(cursor.getString(4));

                    modelList.add(model);
                } while (cursor.moveToNext());
            }
        }


        Log.d("student data", modelList.toString());


        return modelList;
    }


    /*delete a row from database*/

    public void deleteARow(int id){
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete("todo_table", "id" + " = ?", new String[] {String.valueOf(id)});
       db.close();
    }
//
//    public void updateRow(DatabaseModel m)
////    {
////        SQLiteDatabase db=this.getWritableDatabase();
////        ContentValues cv=new ContentValues();
////        cv.put("todo_title",m.getTodoTitle());
////        db.update(TODO_TABLE,cv,"id="+m.getId(),null);
////
////    }



}
