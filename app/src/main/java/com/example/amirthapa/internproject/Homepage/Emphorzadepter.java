package com.example.amirthapa.internproject.Homepage;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.amirthapa.internproject.R;

import java.util.ArrayList;

/**
 * Created by amir thapa on 2/26/2017.
 */

public class Emphorzadepter extends RecyclerView.Adapter<Emphorzadepter.MyViewHolder> {

    private ArrayList<Emparray> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView emppic;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.emppic = (RoundedImageView) itemView.findViewById(R.id.employepic);


        }
    }

    public Emphorzadepter(ArrayList<Emparray> data) {
        this.dataSet = data;
    }

    @Override
    public Emphorzadepter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employeepic, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        Emphorzadepter.MyViewHolder myViewHolder = new Emphorzadepter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final Emphorzadepter.MyViewHolder holder, final int listPosition) {

        RoundedImageView emppic = holder.emppic;


        emppic.setImageResource(dataSet.get(listPosition).getEmpimage());

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }}


